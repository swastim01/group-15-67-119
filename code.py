COMMA = ","

def parse_line(line: str) -> list:
    name, *marks = line.strip().split(COMMA)
    marks = [int(_) for _ in marks]
    return [name] + marks

def load_data(mark_file: str) -> list:
    return [parse_line(line) for line in open(mark_file)]

def is_greater(student_a: list, student_b: list) -> bool:
    for a, b in zip(student_a[1:], student_b[1:]):
        if a <= b:
            return False
    return True

def generate_ranking(mark_list: list) -> list:
    ordered_groups = []
    for student in mark_list:
        placed = False
        for group in ordered_groups:
            if is_greater(student, group[-1]):
                group.append(student)
                placed = True
                break
            elif is_greater(group[-1], student):
                group.insert(0, student)
                placed = True
                break
        if not placed:
            ordered_groups.append([student])
    
    results = [" < ".join([student[0] for student in group]) for group in ordered_groups]
    return results

ranking = generate_ranking(load_data("studentRanking.txt"))
for rank in ranking:
    print(rank)

